#ifndef JT_TUNER_H
#define JT_TUNER_H

#include <QApplication>
#include <QMainWindow>
#include <QDesktopWidget>
#include <QFileSystemWatcher>
#include <QDateTime>
#include <QTimer>
#include <QProcess>
#include <QTextStream>
#include <QFile>
#include <QDir>
#include <unistd.h>


namespace Ui {
class jt_tuner;
}

class jt_tuner : public QMainWindow
{
    Q_OBJECT

public:
    explicit jt_tuner(QWidget *parent = 0);
    ~jt_tuner();
private slots:

    void sre();

    void readsre();

    void stopsre();

    void parse();

    void parse_needs();

    void parse_number();  // this is for channel

    //void parse_store();

    void parse_adapter();

    void parse_program();

    void greet();

    void flitetalk();

    void setup_boswasi();

    void set_volume();

    void set_speaker();

    void play_tuner();

    void ch_to_freq();

    void change_service();

    void exit_tuner();

private:

    int num_result;
    int num_ty;
    int num_digit;
    int volume;
    int channel;  // voice number 2-51 maybe 83
    int adapter;
    int program; //program number
    int talker_flag=0;




    QString asrline;  // This is a global string for speech recognition engine
    QString ttsline;  // This is the string that TTS will say.
    QString ttstemp;
    QString homepath;
    QString filepath;
    QString voice;
    QString load_text;
    QString ch_freq;


    Ui::jt_tuner *ui;
};

#endif // JT_TUNER_H







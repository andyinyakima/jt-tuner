#include "jt_tuner.h"
#include "ui_jt_tuner.h"

/*
 * jt_tuner -> being of sorts with a simple intelligence
 *
 * jt_tuner takes a lot code from jt, so it's version numbers should
 * be low, at least for a while
 *
 * version .01 is preliminary setup
 * version .10 is very functional
 * version .11 removed number box, added keyword load
 * version .12 modify change to next
 * version .13 modify channel to play with what is in the channel box
 */

// placement of the QObject and QProcess is critical
// it has to be in the include section...continued below
QObject *parent;

QProcess *asrin = new QProcess (parent);

// .....and above this section

jt_tuner::jt_tuner(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::jt_tuner)
{

    homepath=QDir::homePath();

    voice="/home/andy/cmu_us_slt.flitevox";

 ui->setupUi(this);
 ui->asr_lineEdit->clear();
 ui->asr_lineEdit->insert("This is Beta version 0.13 of jt-tuner!");
 ttstemp=ui->asr_lineEdit->text();


 QFile filein1(homepath+"/jt-tuner/KEYWORDS");
 filein1.open(QIODevice::ReadOnly | QIODevice::Text);

 QTextStream streamin1(&filein1);
 while (!streamin1.atEnd()) {
           load_text = streamin1.readLine();
       //    ui->textBrowser->clear();
           ui->textBrowser->append(load_text);
}


 //ui->alarm_lineEdit->insert(alarm);
 //setup_announce();



ttsline=ui->asr_lineEdit->text();


      QFile filein(homepath+"/bos-jt/voice.txt");
      filein.open(QIODevice::ReadOnly | QIODevice::Text);

      QTextStream streamin(&filein);
      while (!streamin.atEnd()) {
                voice = streamin.readLine();
                voice.prepend(homepath+"/");

     }




// voice="/home/andy/cmu_us_fem.flitevox";
 //talker_flag=0;
// listen = 0;

 greet();

 sre();


}

jt_tuner::~jt_tuner()
{
    delete ui;
    asrin->kill(); // this is needed to kill instance of julius
                   // other wise there will be too many copies.
}

void jt_tuner::sre()
{
   // let's set up julius
    QString confpath = QDir::homePath();
    confpath.append("/jt-tuner.jconf");

    QString prog = "julius";
    QString conf = "-C";

    QStringList argu;


    argu<<conf<<confpath;


  //  QProcess *asrin = new QProcess(this);
    asrin->setEnvironment(QProcess::systemEnvironment());
    asrin->setProcessChannelMode(QProcess::MergedChannels);

    asrin->start(prog,argu);


    connect(asrin,SIGNAL(readyReadStandardOutput()),this,SLOT(readsre()));
    connect(asrin,SIGNAL(finished(int)),this,SLOT(stopsre()));

}

void jt_tuner::readsre()
{
    int foundstart;
    int foundend;

    QString line;
    QByteArray pile;
    QStringList lines;

    QProcess *asrin = dynamic_cast<QProcess *>(sender());


    if(asrin)
    {

       pile=asrin->readAllStandardOutput();
       lines=QString(pile).split("\n");
       foreach (line, lines) {
           if(line.contains("sentence1"))
           {
               foundstart = line.indexOf("<s>")+3;
               foundend = line.indexOf("</s>");
               line.resize(foundend);
               line.remove(0,foundstart);

               asrline=line;

           ui->asr_lineEdit->insert(asrline);
           }

       }
        ui->asr_lineEdit->insert(asrin->readAllStandardOutput());

    }
    parse();
}

void jt_tuner::stopsre()
{
    asrin->waitForFinished();
    if(asrin->state()!= QProcess::NotRunning)
        asrin->kill();
}


void jt_tuner::parse()
{

    if(asrline.contains("PLAY")==true)
    {
        asrline.remove(0,4);
        play_tuner();
    }

    if(asrline.contains("ADAPTER")==true)
    {
            asrline.remove(0,8);
            parse_adapter();
    }

    if(asrline.contains("CHANNEL")==true)
    {
            asrline.remove(0,8);
            parse_number();
    }

    if(asrline.contains("PROGRAM")==true)
    {
            asrline.remove(0,8);
            parse_program();
    }

    if(asrline.contains("EXIT")==true)
    {
            asrline.remove(0,5);
            exit_tuner();
    }

    if(asrline.contains("VOLUME")==true)
    {
        //asrline.remove(0,7);
        set_volume();
    }

    if(asrline.contains("NEXT")==true)
    {
           asrline.remove(0,5);
           change_service();
    }

    if(asrline.contains("BOS")==true)
    {
           asrline.remove(0,4);
           parse_needs();
    }

    if(asrline.contains("SPEAKER")==true)
    {
        asrline.remove(0,8);
        set_speaker();
    }
    else
    {
        //ui->jt_tuner_lineEdit->clear();
        ui->asr_lineEdit->insert(asrline);


        greet();

    }

}


void jt_tuner::parse_needs()
{
    QString line = asrline;
    asrline.clear();


    if(line.contains("WA")==true)
        setup_boswasi();
}


void jt_tuner::parse_number()
{
    num_digit=0;
    num_ty=0;
    QString line = asrline;
    bool ok;


        line.replace("POINT",".");


        if(line.contains("TWENTY")==true)
        {
           line.remove(0,7);
           num_ty = 20;

        }
        if(line.contains("THIRTY")==true)
        {
           line.remove(0,7);
           num_ty = 30;
        }
        if(line.contains("FOURTY")==true)
        {
            line.remove(0,7);
            num_ty = 40;
        }
        if(line.contains("FIFTY")==true)
        {
           line.remove(0,6);
           num_ty = 50;
        }
        if(line.contains("SIXTY")==true)
        {
           line.remove(0,6);
           num_ty = 60;

        }
/*
        if(line.contains("SEVENTY")==true)
        {
           line.remove(0,8);
           num_ty = 70;
        }
        if(line.contains("EIGHTY")==true)
        {
            line.remove(0,7);
            num_ty = 80;
        }
        if(line.contains("NINETY")==true)
        {
           line.remove(0,7);
           num_ty = 90;
        }
*/
//           line.replace("POINT",".");

           line.replace("THIRTEEN",QString::number(13));
           line.replace("FOURTEEN",QString::number(14));
           line.replace("FIFTEEN",QString::number(15));
           line.replace("SIXTEEN",QString::number(16));
           line.replace("SEVENTEEN",QString::number(17));
           line.replace("EIGHTEEN",QString::number(18));
           line.replace("NINETEEN",QString::number(19));

           line.replace("ZERO",QString::number(0));
      //     line.replace("OTT",QString::number(0));
      //     line.replace("OH",QString::number(0));
           line.replace("ONE",QString::number(1));
           line.replace("TWO",QString::number(2));
           line.replace("THREE",QString::number(3));
           line.replace("FOUR",QString::number(4));
           line.replace("FIVE",QString::number(5));
           line.replace("SIX",QString::number(6));
           line.replace("SEVEN",QString::number(7));
           line.replace("EIGHT",QString::number(8));
           line.replace("NINE",QString::number(9));

           line.replace("TEN",QString::number(10));
           line.replace("ELEVEN",QString::number(11));
           line.replace("TWELVE",QString::number(12));



           line.replace(" ","");
           num_digit = line.toInt(&ok);
           num_result = num_ty + num_digit;
         //  num_result=num_digit;
           line=QString::number(num_result);

           ui->channel_lineEdit->clear();
           ui->channel_lineEdit->insert(line);
           channel=num_result;
           ch_to_freq();

            ttsline=line+" has been selected!";
            ttsline.prepend("RF channel ");
           greet();
}

void jt_tuner::parse_adapter()
{
    num_digit=0;
    num_ty=0;
    QString line = asrline;
    bool ok;


        line.replace("POINT",".");


        if(line.contains("TWENTY")==true)
        {
           line.remove(0,7);
           num_ty = 20;

        }
        if(line.contains("THIRTY")==true)
        {
           line.remove(0,7);
           num_ty = 30;
        }
        if(line.contains("FOURTY")==true)
        {
            line.remove(0,7);
            num_ty = 40;
        }
        if(line.contains("FIFTY")==true)
        {
           line.remove(0,6);
           num_ty = 50;
        }
        if(line.contains("SIXTY")==true)
        {
           line.remove(0,6);
           num_ty = 60;

        }


           line.replace("THIRTEEN",QString::number(13));
           line.replace("FOURTEEN",QString::number(14));
           line.replace("FIFTEEN",QString::number(15));
           line.replace("SIXTEEN",QString::number(16));
           line.replace("SEVENTEEN",QString::number(17));
           line.replace("EIGHTEEN",QString::number(18));
           line.replace("NINETEEN",QString::number(19));

           line.replace("ZERO",QString::number(0));
           line.replace("ONE",QString::number(1));
           line.replace("TWO",QString::number(2));
           line.replace("THREE",QString::number(3));
           line.replace("FOUR",QString::number(4));
           line.replace("FIVE",QString::number(5));
           line.replace("SIX",QString::number(6));
           line.replace("SEVEN",QString::number(7));
           line.replace("EIGHT",QString::number(8));
           line.replace("NINE",QString::number(9));

           line.replace("TEN",QString::number(10));
           line.replace("ELEVEN",QString::number(11));
           line.replace("TWELVE",QString::number(12));



           line.replace(" ","");
           num_digit = line.toInt(&ok);
           num_result = num_ty + num_digit;
         //  num_result=num_digit;
           line=QString::number(num_result);

           ui->adapter_lineEdit->clear();
           ui->adapter_lineEdit->insert(line);



           ttsline=line+" has been selected!";
           ttsline.prepend("Adapter ");

           greet();
}

void jt_tuner::parse_program()
{
    num_digit=0;
    num_ty=0;
    QString line = asrline;
    bool ok;


        line.replace("POINT",".");


        if(line.contains("TWENTY")==true)
        {
           line.remove(0,7);
           num_ty = 20;

        }
        if(line.contains("THIRTY")==true)
        {
           line.remove(0,7);
           num_ty = 30;
        }
        if(line.contains("FOURTY")==true)
        {
            line.remove(0,7);
            num_ty = 40;
        }
        if(line.contains("FIFTY")==true)
        {
           line.remove(0,6);
           num_ty = 50;
        }
        if(line.contains("SIXTY")==true)
        {
           line.remove(0,6);
           num_ty = 60;

        }

           line.replace("THIRTEEN",QString::number(13));
           line.replace("FOURTEEN",QString::number(14));
           line.replace("FIFTEEN",QString::number(15));
           line.replace("SIXTEEN",QString::number(16));
           line.replace("SEVENTEEN",QString::number(17));
           line.replace("EIGHTEEN",QString::number(18));
           line.replace("NINETEEN",QString::number(19));

           line.replace("ZERO",QString::number(0));

           line.replace("ONE",QString::number(1));
           line.replace("TWO",QString::number(2));
           line.replace("THREE",QString::number(3));
           line.replace("FOUR",QString::number(4));
           line.replace("FIVE",QString::number(5));
           line.replace("SIX",QString::number(6));
           line.replace("SEVEN",QString::number(7));
           line.replace("EIGHT",QString::number(8));
           line.replace("NINE",QString::number(9));

           line.replace("TEN",QString::number(10));
           line.replace("ELEVEN",QString::number(11));
           line.replace("TWELVE",QString::number(12));



           line.replace(" ","");
           num_digit = line.toInt(&ok);
           num_result = num_ty + num_digit;
         //  num_result=num_digit;
           line=QString::number(num_result);

           ui->program_lineEdit->clear();
           ui->program_lineEdit->insert(line);

           if(num_result==0)
           {
               ttsline= "Since program 0 is selected all services can be seen on this station";
           }
           else if(num_result!=0)
           {
                ttsline=line+" has been selected";
                ttsline.prepend("Program ");
           }
           greet();
}

void jt_tuner::greet()
{

    if(ui->ttson_checkBox->isChecked()==true)
            flitetalk();
 // if(talker_flag==0)
   // flitetalk();

}

void jt_tuner::flitetalk()
{

    QString prg = "padsp"; //padsp was added so oss systems will play flite 2.0
    QString prog = "flite";
    QString readfile ="-t";
    QString voicecall = "-voice";
    QString voicename;
    QString setstuff = "--seti";
    QStringList argu;


    argu<<prog<<voicecall<<voice<<setstuff<<readfile<<ttsline;
   // ttstemp=ttsline;
    ttsline.clear();
   // argu<<asrline;

    QProcess *flt1 = new QProcess(this);
        flt1->start(prg,argu);


    sleep(3);
}

void jt_tuner::setup_boswasi()
{
        ttsline = "Leaving Tuner to go Boswasi! ";
        greet();
        sleep(3);

        QProcess *boswasi_call = new QProcess (this);
        QString prog = "boswasi";

        boswasi_call->startDetached(prog);
        sleep(3);
        QCoreApplication::exit();

}

void jt_tuner::set_volume()
{
    QString line =asrline;
    asrline.clear();

    QString prog = "amixer";
    QString dee = "-D";
    QString pulse = "pulse";
    QString sset = "sset";
    QString master = "Master";
    QString percent = "%";

    QStringList argu;

    if(line.contains("BOTTOM")==true)
    {
        volume=1;
    }

    else if(line.contains("NOMINAL")==true)
    {
        volume=75;
    }

    else if(line.contains("TOP")==true)
    {
        volume=100;
    }

    else if(line.contains("INCREASE")==true)
    {
        volume=volume+10;
    }

    else if(line.contains("HALF")==true)
    {
        volume=50;
    }
    percent.prepend(QString::number(volume));

    argu<<dee<<pulse<<sset<<master<<percent;

    ui->asr_lineEdit->clear();
    ui->asr_lineEdit->insert("Volume = ");
    ui->asr_lineEdit->insert(percent);


    QProcess *vol_call = new QProcess(this);

    vol_call->start(prog,argu);

   // ttsline = "Volume ";
    ttsline = (ui->asr_lineEdit->text());

    greet();

}

void jt_tuner::play_tuner()
{
    // vlc atsc://frequency=623000000 --program=1 --dvb-adapter=1
    bool ok;
    QString chn;
    QString prog = "vlc";
    QString atsc = "atsc://frequency=";
    QString prog_num = "--program=";
    QString adap_num = "--dvb-adapter=";
    QString budget = "--dvb-budget-mode";

    QStringList argu;

    if(asrline.contains("TUNER")==true)
    {
        chn=ui->channel_lineEdit->text();
        channel=chn.toInt(&ok);
        ch_to_freq();
        atsc.append(ch_freq);
        prog_num.append(ui->program_lineEdit->text());
        adap_num.append(ui->adapter_lineEdit->text());

        if(ui->program_lineEdit->text()=="0")
        {
            argu<<atsc<<adap_num<<budget;
        }
        else
            argu<<atsc<<prog_num<<adap_num;

        QProcess *tuner_call = new QProcess(this);

        tuner_call->start(prog,argu);

        ttsline="There you go";
        greet();
    }

}

void jt_tuner::ch_to_freq()
{
    int freq;

    if(channel==2)
        ch_freq="57000000";
    if(channel==3)
        ch_freq="63000000";
    if(channel==4)
        ch_freq="69000000";
    if(channel==5)
        ch_freq="79000000";
    if(channel==6)
        ch_freq="85000000";

    if(channel>=7 && channel<=13)
    {
        freq=(channel*6)+135;

        ch_freq = QString::number(freq);
        ch_freq.append("000000");
    }

     if(channel>=14 && channel<=51)
     {
         freq=(channel*6)+389;

         ch_freq = QString::number(freq);
         ch_freq.append("000000");
     }

}

void jt_tuner::change_service()
{
    QString prog = "xdotool";
    QStringList argu;
    QString ht_kys = "shift+x";
    QString ky = "key";

    QString line = asrline;
    asrline.clear();

    if(line.contains("SERVICE")==true)
    {
        argu<<ky<<ht_kys;

        QProcess *ch_chnge = new QProcess(this);

        ch_chnge->start(prog,argu);

        ttsline="Changing service";
        greet();

    }

}

void jt_tuner::exit_tuner()
{
    QString prog = "xdotool";
    QStringList argu;
    QString ht_kys = "ctrl+q";
    QString ky = "key";

    QString line = asrline;
    asrline.clear();

    if(line.contains("SERVICE")==true)
    {
        argu<<ky<<ht_kys;

        QProcess *ch_chnge = new QProcess(this);

        ch_chnge->start(prog,argu);

        ttsline="Exit the Tuner!";
        greet();

    }


}

void jt_tuner::set_speaker()
{
    QString line =asrline;
    asrline.clear();


    if(line.contains("OFF")==true)
    {

        ui->ttson_checkBox->setChecked(false);
    }

    else if(line.contains("ON")==true)
    {
        ui->ttson_checkBox->setChecked(true);
    }

    ttsline = "Speaker On ";

    greet();

}

#-------------------------------------------------
#
# Project created by QtCreator 2016-12-28T16:24:56
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = jt-tuner
TEMPLATE = app


SOURCES += main.cpp\
        jt_tuner.cpp

HEADERS  += jt_tuner.h

FORMS    += jt_tuner.ui
